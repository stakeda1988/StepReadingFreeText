//
//  ResultViewController.swift
//  StepReading
//
//  Created by SHOKI TAKEDA on 11/3/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var showCountSecond: UILabel!
    var timer:NSTimer!
    var startCount = true
    var counter:Double = 0
    
    @IBAction func pauseButton(sender: UIButton) {
        timer.invalidate()
        startButton.hidden = false
        pauseLabel.hidden = true
    }
    @IBOutlet weak var pauseLabel: UIButton!
    @IBAction func saveLearning(sender: UIButton) {
    }
    @IBAction func startSentences(sender: UIButton) {
        englishLabel.attributedText = globalEnglishLabel[0]
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("onUpdate"), userInfo: nil, repeats: true)
        startButton.hidden = true
        pauseLabel.hidden = false
    }
    
    func wordCount(s: String) -> Array<String> {
        let separators = NSCharacterSet(charactersInString: ".")
        var words = s.componentsSeparatedByCharactersInSet(separators)
        for i in 0...words.count-1 {
            words[i] = words[i] + ". "
        }
        return words
    }
    
    func smallWordCounter(s: String) -> Int {
        let words = s.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        return words.count
    }
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    
    let texts:NSArray = ["-150wps- TOEIC500Lv.", "-250wps- TOEIC600Lv.", "-300wps- TOEIC750Lv.", "-350wps- TOEIC850Lv.", "-450wps- TOEIC990Lv.", "-550wps-", "-650wps-" ]
    var speedLevel:Int = 150
    
    var arrayCharacter = [String]()
    var countArray = [Int]()
    var tmpCounter = [Double]()
    var colour = [UIColor]()
    var myString = [NSMutableAttributedString]()
    var totalTimeCounter = [Double]()
    var tmpTotalTimeCounter = Int()
    var globalEnglishLabel = [NSMutableAttributedString]()
    var mainRow:Int = 0
    var mainEnglishText:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        
        saveButton.hidden = true
        pauseLabel.hidden = true
        englishLabel.hidden = false
        englishLabel.lineBreakMode = NSLineBreakMode.ByCharWrapping
        englishLabel.text = mainEnglishText
        
        showCountSecond.text = String(Int(counter))
        arrayCharacter = wordCount(englishLabel.text!)
        countArray = [Int](count:arrayCharacter.count, repeatedValue:0)
        tmpCounter = [Double](count:arrayCharacter.count, repeatedValue:0)
        colour = [UIColor](count:arrayCharacter.count, repeatedValue:UIColor())
        myString = [NSMutableAttributedString](count:arrayCharacter.count, repeatedValue:NSMutableAttributedString())
        totalTimeCounter = [Double](count:arrayCharacter.count, repeatedValue:0)
        globalEnglishLabel = [NSMutableAttributedString](count:arrayCharacter.count, repeatedValue:NSMutableAttributedString())
        for i in 0...arrayCharacter.count-1 {
            countArray[i] = smallWordCounter(arrayCharacter[i])
            tmpCounter[i] = Double(countArray[i]*60)/Double(speedLevel)
            if i == 0 {
                totalTimeCounter[i] = tmpCounter[i]
                for j in 0...arrayCharacter.count-1 {
                    if i == j {
                        colour[0] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[0] ]
                        myString[0] = NSMutableAttributedString(string: arrayCharacter[0], attributes: myAttribute)
                    } else {
                        colour[j] = UIColor(red:255/255, green:248/255, blue:220/255, alpha:0.1)
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    }
                }
                globalEnglishLabel[0] = myString[0]
            } else {
                totalTimeCounter[i] = totalTimeCounter[i-1] + tmpCounter[i]
                for j in 0...arrayCharacter.count-1 {
                    if j == 0 {
                        colour[j] = UIColor(red:255/255, green:248/255, blue:220/255, alpha:0.1)
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                    } else if i == j {
                        colour[j] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    } else {
                        colour[j] = UIColor(red:255/255, green:248/255, blue:220/255, alpha:0.1)
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    }
                }
                globalEnglishLabel[i] = myString[0]
            }
        }
        
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    func onUpdate(){
        counter += 1
        showCountSecond.text = String(Int(counter))
        for i in 0...arrayCharacter.count-1 {
            if i == 0{
                if counter < totalTimeCounter[0] {
                    englishLabel.attributedText = globalEnglishLabel[0]
                }
            } else if i != 0 {
                if counter >= totalTimeCounter[i-1] && counter < totalTimeCounter[i]{
                    englishLabel.attributedText = globalEnglishLabel[i]
                    if i == arrayCharacter.count-2 {
                        timer.invalidate()
                        showCountSecond.text = "finish"
                    }
                }
            }
        }
    }
    
    //表示列
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //表示個数
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return texts.count
    }
    
    //表示内容
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return texts[row] as! String
    }
    
    //選択時
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        speedLevel = row
        switch row {
        case 0 :
            speedLevel = 150
        case 1 :
            speedLevel = 250
        case 2 :
            speedLevel = 300
        case 3 :
            speedLevel = 350
        case 4 :
            speedLevel = 450
        case 5 :
            speedLevel = 550
        case 6 :
            speedLevel = 650
        default:
            speedLevel = 150
        }
        
        for i in 0...arrayCharacter.count-1 {
            countArray[i] = smallWordCounter(arrayCharacter[i])
            tmpCounter[i] = Double(countArray[i]*60)/Double(speedLevel)
            if i == 0 {
                totalTimeCounter[i] = tmpCounter[i]
                for j in 0...arrayCharacter.count-1 {
                    if i == j {
                        colour[0] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[0] ]
                        myString[0] = NSMutableAttributedString(string: arrayCharacter[0], attributes: myAttribute)
                    } else {
                        colour[j] = UIColor(red:255/255, green:248/255, blue:220/255, alpha:0.1)
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    }
                }
                globalEnglishLabel[0] = myString[0]
            } else {
                totalTimeCounter[i] = totalTimeCounter[i-1] + tmpCounter[i]
                for j in 0...arrayCharacter.count-1 {
                    if j == 0 {
                        colour[j] = UIColor(red:255/255, green:248/255, blue:220/255, alpha:0.1)
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                    } else if i == j {
                        colour[j] = UIColor.whiteColor()
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    } else {
                        colour[j] = UIColor(red:255/255, green:248/255, blue:220/255, alpha:0.1)
                        let myAttribute = [ NSFontAttributeName: UIFont(name: "American Typewriter", size: 16.0)!, NSForegroundColorAttributeName: colour[j] ]
                        myString[j] = NSMutableAttributedString(string: arrayCharacter[j], attributes: myAttribute)
                        myString[0].appendAttributedString(myString[j])
                    }
                }
                globalEnglishLabel[i] = myString[0]
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
